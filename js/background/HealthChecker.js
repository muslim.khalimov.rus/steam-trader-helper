"use strict";

export class HealthChecker {
    constructor(steam, trader, version) 
    {
        this.version = version;
        this._steam = steam;
        this._trader = trader;
        this._checkList = {
            steamAccountIsAuthorized: false,
            traderAccountIsAuthorized: false,
            steamApiKeyIsRegistered: false,
            steamApiKeyIsSavedOnTraderAccount: false,
            //traderWSIsConnected: false,
            extensionVersionIsUpToDated: false,
        };
        this._errorMessages = {
            steamAccountIsAuthorized: 'Авторизуйтесь в steam: <a target="_blank" href="https://steamcommunity.com/my/inventory/">https://steamcommunity.com/</a>.',
            traderAccountIsAuthorized: 'Авторизуйтесь в стим трейдере через в тот же аккаунт, что и в steam: <a target="_blank" href="https://steam-trader.com/login/">https://steam-trader.com/login/</a>.',
            steamApiKeyIsRegistered: 'Авторизуйтесь в Steam и создайте steam апи ключ вручную, если расширению не удалось сделать это автоматически: <a target="_blank" href="https://steamcommunity.com/dev/apikey">https://steamcommunity.com/dev/apikey</a>.',
            steamApiKeyIsSavedOnTraderAccount: 'Сохраните steam апи ключ в стим трейдере (Для этого авторизуйтесь в стиме и стим трейдере и дождитесь пока расширение само все сделает).',
            //traderWSIsConnected: 'Отсутствует подключение к стим трейдеру. Проверьте свое интернет соединение или обратитесь в техподдержку.',
            extensionVersionIsUpToDated: 'Обновите расширение до последней версии: https://gitlab.com/reeves1/steam-trader-helper',
        };
        this._lastCheckTime = null;
    }
    async check() 
    {
        await this.checkSteam();
        await this.checkTrader();
    }
    async checkSteam() 
    {
        let now = Date.now();

        this._lastCheckTime = now;

        this._checkList.steamAccountIsAuthorized = await this._steam.isAccountAuthorized();
        
        if(!this._checkList.steamAccountIsAuthorized)
            return false;

        this._checkList.steamApiKeyIsRegistered = await this._steam.isApiKeyRegistered();
        
        if(!this._checkList.steamApiKeyIsRegistered){
            this._checkList.steamApiKeyIsRegistered = await this._steam.registerApiKey();
        }
    }
    async checkTrader() 
    {
        this._checkList.traderAccountIsAuthorized = await this._trader.isAuthedWithSteamid(
            this._steam.accountSteamid
        );
        
        if(!this._checkList.traderAccountIsAuthorized)
            return false;

        this._checkList.steamApiKeyIsSavedOnTraderAccount = await this._trader.isSteamApiKeySaved(
            this._steam.accountApiKey
        );

        if(!this._checkList.steamApiKeyIsSavedOnTraderAccount) {
            this._checkList.steamApiKeyIsSavedOnTraderAccount = await this._trader.saveSteamApiKeyOnTraderAccount(
                this._steam.accountApiKey
            );
        }

        //this._checkList.traderWSIsConnected = this._trader.isWSConnected();

        this._checkList.extensionVersionIsUpToDated = this.version == await this._trader.getLastExtensionVersion();
    }
    allIsOk() 
    {
        //this._checkList.traderWSIsConnected = this._trader.isWSConnected();

        return Object.values(this._checkList).every(el => el === true);
    }
    nextError()
    {
        for(const key in this._checkList) {
            if(!this._checkList[key]) {
                return this._errorMessages[key];
            }
        }
    }
    getInfo() 
    {
        return {
            allRight: this.allIsOk(),
            checkList: this._checkList,
            lastCheckTime: this._lastCheckTime,
            nextError: this.nextError(),
        };
    }
}