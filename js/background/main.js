"use strict";

import { SteamAPI } from './SteamAPI.js';
import { TraderAPI } from './TraderAPI.js';
import { HealthChecker } from './HealthChecker.js';
import { TradeOfferManager } from './TradeOfferManager.js';
import { ExtensionManager } from './ExtensionManager.js';

const manifest = chrome.runtime.getManifest();

const steam = new SteamAPI();
const trader = new TraderAPI(manifest.homepage_url);
const health = new HealthChecker(steam, trader, manifest.version);
const offers = new TradeOfferManager(steam, trader, health);
const extension = new ExtensionManager(offers);

trader.pingActiveState(health);


